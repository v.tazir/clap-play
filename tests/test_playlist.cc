#include "../header/test_playlist.h"
#include <fstream>

void test_playlist::setUp(){
  PGconn *conn = PQsetdbLogin("postgresql.bts-malraux72.net", "5432", nullptr, nullptr, "Cours", "v.tazir", "P@ssword");
  if(PQstatus(conn) == CONNEXION_OK){
    CPPUNIT_ASSERT_NO_THROW(p(1, "playlist_test", 5000, "", conn));
  }
 }

/*void test_playlist::constructor()
{
  PGconn *conn = PQsetdbLogin("postgresql.bts-malraux72.net", "5432", nullptr, nullptr, "Cours", "v.tazir", "P@ssword");
  if(PQstatus(conn) == CONNEXION_OK){
    CPPUNIT_ASSERT_NO_THROW(p(1, "playlist_test", 5000, "", conn));
  }
}*/

void test_playlist::m3uExist(){
  p.writeM3U("a", "", "", 2, "");
  std::ifstream file("playlist_test");
  CPPUNIT_ASSERT(file.is_open());
  file.close();
}

void test_playlist::xspfExist(){
  p.writeXSPF("a", "", "", 2, "");
  std::ifstream file("playlist_test");
  CPPUNIT_ASSERT(file.is_open());
  file.close;
}

void test_playlist::testGetId(){
  CPPUNIT_ASSERT_EQUAL(1, p.getId());
}

void test_playlist::testGetDuration(){
  CPPUNIT_ASSERT_EQUAL(5000, p.getDuration());
}

void test_playlist::testSetId(){
  p.setId(-6);
  CPPUNIT_ASSERT_EQUAL(-6, p.getId());
}

void test_playlist::testSetDuration(){
  p.setDuration(14000);
  CPPUNIT_ASSERT_EQUAL(14000, p.getDuration());
}

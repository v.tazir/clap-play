#include <iostream>
#include <gflags/gflags.h>
#include <gflags/gflags_declare.h>

/**
 * \file manage_cli.h
 * \brief File used to manage the CLI
 *
 * Declare all the parameter of the command line
 */

DECLARE_string(db_name);
DECLARE_string(db_host);
DECLARE_string(db_password);
DECLARE_string(db_user);
DECLARE_string(db_schema);
DECLARE_uint64(duration);
DECLARE_string(title);
DECLARE_bool(m3u);
DECLARE_bool(xspf);
DECLARE_string(genre);
DECLARE_string(artist);
DECLARE_string(format);
DECLARE_uint64(polyphony);
DECLARE_string(playlist);
DECLARE_string(config_file);
DECLARE_string(export_file);

std::string config_db_host;

void configure();

#ifndef SUBGENRE_H
#define SUBGENRE_H

#include <string>
#include <libpq-fe.h>

/**
 * \class Subgenre
 * \brief Allow to retrieve the attributes from the database
 */
class Subgenre {
private:
  unsigned int id;
  std::string subgenre;

public:
  /**
   * Unique constructor which retrieve the value of the attributes from the database according to a track
   *
   * \param unsigned int - id_track : id of the track related to the album 
   *
   * \param PGconn * - connexion : handler for the database connexion
   */
  Subgenre(unsigned int, PGconn *);

  /**
   * \brief Destructor
   */
  ~Subgenre();

  /**
   * \brief Get the id of the Subgender
   *
   * \return id : the id of the subgender as an unsigned integer
   */
  unsigned int getId();

  /**
   * \brief Get the Subgender name
   *
   * \return subgenre : the name of the subgender as a std::string
   */
  std::string getSubgenre();
};
#endif //SUBGENRE_H

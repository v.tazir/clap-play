#ifndef TEST_PLAYLIST_H
#define TEST_PLAYLIST_H

#include <cppunit/extensions/HelperMacros.h>
#include <libpq-fe.h>
#include "playlist.h"

class test_playlist : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_playlist);
  CPPUNIT_TEST(m3uExist);
  CPPUNIT_TEST(xspfExist);
  CPPUNIT_TEST(testGetId);
  CPPUNIT_TEST(testGetDuration);
  CPPUNIT_TEST(testSetId);
  CPPUNIT_TEST(testSetDuration);
  CPPUNIT_TEST_SUITE_END();
private:
  Playlist p;
  
public:
  void setUp();
  //void constructor();
  void m3uExist();
  void xspfExist();
  void testGetId();
  void testGetDuration();
  void testSetId();
  void testSetDuration();
};
#endif //TEST_PLAYLIST_H

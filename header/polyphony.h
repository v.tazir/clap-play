#ifndef POLYPHONY_H
#define POLYPHONY_H

#include <libpq-fe.h>

/**
 * \class Polyphony
 * \brief Allow to retrieve the attributes from the database                         
 */
class Polyphony {

private:
  unsigned int id;
  unsigned int number;

public:
  /**
 * Unique constructor which retrieve the value of the attributes from the database according to a track
 *
 * \param unsigned int - id_track : id of the track related to the polyphony 
 *
 * \param PGconn * - connexion : handler for the database connexion
 */
  Polyphony(unsigned int, PGconn *);

  /**
   * \brief Destructor
   */
  ~Polyphony();

  /**
   * \brief Get the id of the Polyphony
   * 
   * \return id : the id of the polyphony as an unsigned integer
   */
  unsigned int getId();

  /**
   * \brief Get the number of the polyphony
   *
   * \return number : the number of the polyphony as an unsigned integer
   */
  unsigned int getNumber();
};

#endif //POLYPHONY_H
